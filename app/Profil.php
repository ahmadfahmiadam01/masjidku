<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profil';

    protected $fillable = ['umur', 'bio', 'alamat', 'users_id'];

    public function users()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
}
