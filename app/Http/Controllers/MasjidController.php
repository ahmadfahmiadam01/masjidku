<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Masjid;
use File;

class MasjidController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masjid = Masjid::all();

        return view('masjid.index', compact('masjid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kota = DB::table('kota')->get();

        return view('masjid.create', compact('kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'sejarah' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'kota_id' => 'required'
        ]);


        $FotoName = time().'.'.$request->foto->extension();

        $request->foto->move(public_path('foto'), $FotoName);

        $masjid = new Masjid;

        $masjid->nama = $request->nama;
        $masjid->alamat = $request->alamat;
        $masjid->sejarah = $request->sejarah;
        $masjid->foto = $FotoName;
        $masjid->kota_id = $request->kota_id;
        

        $masjid->save();

        return redirect('/masjid/create');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masjid = Masjid::findOrFail($id);

        return view('masjid.show', compact('masjid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kota = DB::table('kota')->get();
        $masjid = Masjid::findOrFail($id);

        return view('masjid.edit', compact('masjid', 'kota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'sejarah' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg|max:2048',
            'kota_id' => 'required'
        ]);

        $masjid = Masjid::find($id);

        if($request->has('foto')){
            $FotoName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('foto'), $FotoName);

            $masjid->nama = $request->nama;
            $masjid->alamat = $request->alamat;
            $masjid->sejarah = $request->sejarah;
            $masjid->foto = $FotoName;
            $masjid->kota_id = $request->kota_id;
        }else{
            $masjid->nama = $request->nama;
            $masjid->alamat = $request->alamat;
            $masjid->sejarah = $request->sejarah;
            $masjid->kota_id = $request->kota_id;
        }

        $masjid->update();

        return redirect('/masjid');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masjid = Masjid::find($id);

        $path = "foto/";
        File::delete($path . $masjid->foto);
        $masjid->delete();

        return redirect('/masjid');
    }
}
