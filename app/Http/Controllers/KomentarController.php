<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Komentar;

class KomentarController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'komentar' => 'required'
        ]);
    
        $komentar = new Komentar;
    
        $komentar->komentar = $request->komentar;
        $komentar->users_id = Auth::id();
        $komentar->masjid_id = $request->masjid_id;
    
        $komentar->save();
    
        return redirect()->back();
    }

    
}
