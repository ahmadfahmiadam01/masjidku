<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kota;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class KotaController extends Controller
{
    public function create(){

        return view('kota.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
        ]);

        DB::table('kota')->insert([
            'nama' => $request['nama']
        ]);

        Alert::success('Berhasil', 'Tambah Data Kota Berhasil');

        return redirect('/kota');
    }

    public function index(){
        $kota = Kota::all();
        return view('kota.index', compact('kota'));
    }

    // public function index(){
    //     $kota = DB::table('kota')->get();
    //     return view('kota.index', compact('kota'));
    // }
    
    public function show($id){
        $kota = Kota::find($id);
        return view('kota.show', compact('kota'));
    }

    // public function show($id){
    //     $kota = DB::table('kota')->where('id', $id)->first();
    //     return view('kota.show', compact('kota'));
    // }
    
    // public function edit($kota_id){
    //     $kota = Kota::where('id', $kota_id)->first();
    //     return view('kota.edit', compact('kota'));
    // }

    public function edit($id){
        $kota = DB::table('kota')->where('id', $id)->first();
        return view('kota.edit', compact('kota'));
    }
    
    // public function update(Request $request, $kota_id){
    //         $request->validate([
    //             'nama' => 'required'
    //         ]);

    //         $kota = Kota::find($kota_id);

    //         $kota->nama = $request['nama'];

    //         $kota->save();    
            
    //         return redirect('/kota');
        
    //     }


    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ]);
    
        $query = DB::table('kota')
            ->where('id', $id)
            ->update([
                'nama' =>$request['nama']
            ]);
        
        Alert::success('Update', 'Data Kota Berhasil Di Update');

        return redirect('/kota');
    
    }
    
    // public function destroy($kota_id){
        
    //     $kota = Kota::find($kota_id);

    //     $kota->delete();
    
    //     return redirect('/kota');
    // }

    public function destroy($id){
        DB::table('kota')->where('id', $id)->delete();

        Alert::success('Delete', 'Data Kota Berhasil Di Hapus');
    
        return redirect('/kota');
    }
}
