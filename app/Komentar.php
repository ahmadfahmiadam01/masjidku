<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';

    protected $fillable = ['users_id', 'masjid_id', 'komentar'];

    public function masjid()
    {
        return $this->belongsTo('App\Masjid', 'masjid_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
}
