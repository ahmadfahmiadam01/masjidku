<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masjid extends Model
{
    protected $table = 'masjid';
    
    protected $fillable = ['nama', 'alamat', 'sejarah', 'foto', 'kota_id'];

    public function kota()
    {
        return $this->belongsTo('App\Kota', 'kota_id');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar', 'masjid_id');
    }
    
}
