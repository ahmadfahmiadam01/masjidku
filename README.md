# Final Project

## Kelompok 9

## Anggota Kelompok
* R Radite Satria
* Ahmad Fahmi Adam
* Surya Abdillah

## Tema Project
Masjidku

## ERD
![ERD_FP](/uploads/de79ecf519cf9dfc2f8df1a75c9209c3/ERD_FP.png)

## Link Video
* Link Demo Aplikasi : https://www.youtube.com/watch?v=mUWrXKIKw60
* Link Deploy(Optional) : http://project-masjidku.herokuapp.com/masjid
