<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', 'AuthController@register');

Route::get('/', 'MasjidController@index');

Route::get('/contact', 'ContactController@contact');

// CRUD MASJID
Route::resource('masjid', 'MasjidController');

// Auth
Route::group(['middleware' => ['auth']], function () {

// Update Profil
Route::resource('profil', 'ProfilController')->only([
    'index', 'update'
]);

Route::resource('komentar', 'KomentarController')->only([
    'index', 'store'
]);

});

// CRUD Kota
Route::get('/kota/create','KotaController@create');
Route::get('/kota/{kota_id}/edit', 'KotaController@edit');
Route::post('/kota', 'KotaController@store');
Route::get('/kota', 'KotaController@index');
Route::get('/kota/{kota_id}', 'KotaController@show');
Route::put('/kota/{kota_id}', 'KotaController@update');
Route::delete('/kota/{kota_id}', 'KotaController@destroy');

Auth::routes();

