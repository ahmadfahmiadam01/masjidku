@extends('layout.master')

@section('judul')
Update Profil
@endsection
 
@push('script')
<script src="https://cdn.tiny.cloud/1/4dpghq2obz90n5ffmt3eaqnm8fd218lca60os5hu37ttmtda/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      toolbar_mode: 'floating'
    });
  </script>

@endpush

@section('content')

<form action="/profil/{{$profil->id}}" method="POST">

    @csrf
    @method('PUT')
    
    <div class="form-group">
        <label>Nama User</label>
        <input type="text" value="{{$profil->users->name}}" class="form-control" disabled>
    </div>
    <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profil->users->email}}" class="form-control" disabled>
    </div>

    <div class="form-group">
    <label>Umur Profil</label>
        <input type="number" name="umur" value="{{$profil->umur}}" class="form-control">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control">{{$profil->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" id="">{{$profil->alamat}}</textarea>
        </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection