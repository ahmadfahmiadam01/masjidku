@extends('layout.master')

@section('judul')
Detail {{$masjid->nama}}
@endsection
    
@section('content')


<h1>{{$masjid->nama}} ({{$masjid->kota->nama}})</h1>
<p>Alamat : {{$masjid->alamat}}<p>
<img src="{{asset('foto/'. $masjid->foto)}}" alt="">
<p align="justify ">{{$masjid->sejarah}}</p>

<h1>Komentar</h1>


@foreach ($masjid->komentar as $item)
    <div class="card">
        <div class="card-body">
            <small><b>{{$item->users->name}}</b></small>
            <p class="card-text">{{$item->komentar}}</p>

        </div>

    </div>
@endforeach



<form action="/komentar" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf

    <div class="form-group">
        <label>Komentar</label>
        <input type="hidden" name="masjid_id" value="{{$masjid->id}}">
        <textarea name="komentar" class="form-control"></textarea>
    </div>
    @error('komentar')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>


<a href="/masjid" class="btn btn-secondary">Kembali</a>

@endsection