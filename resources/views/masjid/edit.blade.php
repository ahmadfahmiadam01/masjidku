@extends('layout.master')

@section('judul')
Edit Masjid
@endsection
    
@section('content')

<form action="/masjid/{{$masjid->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Masjid</label>
      <input type="text" value="{{$masjid->nama}}" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <input type="text" name="alamat" class="form-control">
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Sejarah</label>
        <textarea name="sejarah" class="form-control" cols="30" rows="10">{{$masjid->sejarah}}</textarea>
    </div>
    @error('sejarah')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Foto</label>
        <input type="file" name="foto" class="form-control">
    </div>
    @error('foto')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Kota</label>
        <select name="kota_id" class="form-control" id="">
            <option value="">---Pilih Kota---</option>
            @foreach ($kota as $item)
                @if ($item->id === $masjid->kota_id)
                    
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>                    
                @endif                
            @endforeach
        </select>
    </div>
    @error('kota_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection