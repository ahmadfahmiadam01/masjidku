@extends('layout.master')

@section('judul')
List Masjid
@endsection
    
@section('content')

@auth
<a href="/masjid/create" class="btn btn-primary my-3">Tambah Masjid</a>
@endauth

<div class="row">
    @forelse ($masjid as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('foto/'. $item->foto)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <span class="badge badge-info">{{$item->kota->nama}}</span>
              <h3>{{$item->nama}}</h3>
              <p class="card-text">{{Str::limit($item->sejarah, 100)}}</p>
              
              @auth
              <form action="/masjid/{{$item->id}}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/masjid/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/masjid/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
              </form>
              @endauth

              @guest
              <a href="/masjid/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              @endguest

            </div>
        </div>
    </div>
    @empty
        <h4>Data Masjid Belum Ada</h4>
    @endforelse
    
</div>

@endsection