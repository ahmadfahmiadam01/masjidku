@extends('layout.master')

@section('judul')
Edit Kota
@endsection
    
@section('content')

<form action="/kota/{{$kota->id}}" method="POST">
    @csrf
    @method('PUT')
    
    <div class="form-group">
      <label>Kota</label>
      <input type="text" name="nama" value="{{$kota->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection