@extends('layout.master')

@section('judul')
Tambah Kota
@endsection
    
@section('content')

@auth
<a href="/kota/create" class="btn btn-secondary mb-3">Tambah Kota</a>
@endauth

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Kota</th>
        <th scope="col">List Masjid</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    
    <tbody>
        @forelse ($kota as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <ul>
                        @foreach ($item->masjid as $value)
                            <li>{{$value->nama}}</li>
                        @endforeach
                        
                    </ul>
                </td>
                <td>
                    
                    <form action="/kota/{{$item->id}}" method="POST">
                        <a href="/kota/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        
                        @auth    
                        <a href="/kota/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        @endauth
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
</table>
@endsection