@extends('layout.master')

@section('judul')
Detail Kota
@endsection
    
@section('content')

<h1>{{$kota->nama}}</h1>

<div class="row">
    @foreach ($kota->masjid as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('foto/'. $item->foto)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3>{{$item->nama}}</h3>
                    <p class="card-text"> {{($item->sejarah)}} </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection