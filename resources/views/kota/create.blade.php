@extends('layout.master')

@section('judul')
Tambah Kota
@endsection
@section('content')

<form action="/kota"method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Kota</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
